//
//  DevOpsPractical3App.swift
//  DevOpsPractical3
//
//  Created by Divyesh Vekariya on 06/01/24.
//

import SwiftUI

@main
struct DevOpsPractical3App: App {
    var body: some Scene {
        WindowGroup {
            UserListView()
        }
    }
}
