//
//  UserAlbumModel.swift
//  DevOpsPractical3
//
//  Created by Divyesh Vekariya on 06/01/24.
//

import Foundation

struct UserAlbumAPIResponse: Codable, Hashable {
    let userId: Int
    let id: Int
    let title: String
}
