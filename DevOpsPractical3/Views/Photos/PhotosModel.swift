//
//  PhotosModel.swift
//  DevOpsPractical3
//
//  Created by Divyesh Vekariya on 06/01/24.
//

import Foundation

struct PhotosAPIResponse: Codable, Hashable {
    var albumId: Int
    var id: Int
    var title: String
    var url: String
    var thumbnailUrl: String
}
