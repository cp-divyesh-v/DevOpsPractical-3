//
//  UserListAPIResponse.swift
//  DevOpsPractical3
//
//  Created by Divyesh Vekariya on 06/01/24.
//

import Foundation

struct UserListAPIResponse: Codable, Hashable {
    let id: Int
    let name: String
    let username: String
    let email: String
}
